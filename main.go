package main

import (
	"log"
	"os"

	"gitlab.com/slbmax/flow-rosetta/configuration"
	"gitlab.com/slbmax/flow-rosetta/examples/ethereum/client"
	sdkTypes "gitlab.com/slbmax/flow-rosetta/types"
	"gitlab.com/slbmax/flow-rosetta/utils"
)

func main() {
	cfg, err := configuration.DefaultConfiguration()
	if err != nil {
		log.Fatalln("unable to load configuration: %w", err)
	}

	// Load all the supported operation types, status
	types := sdkTypes.LoadTypes()
	errors := sdkTypes.Errors

	// Create a new ethereum client by leveraging SDK functionalities
	client, err := client.NewEthereumClient(cfg)
	if err != nil {
		log.Fatalln("cannot initialize client: %w", err)
	}

	log.SetOutput(os.Stdout)
	log.Printf("Starting Rosetta server on port %d\n", cfg.Port)

	// Bootstrap to start the Rosetta API server
	err = utils.BootStrap(cfg, types, errors, client)
	if err != nil {
		log.Fatalln("unable to bootstrap Rosetta server: %w", err)
	}
}
