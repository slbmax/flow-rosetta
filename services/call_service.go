package services

import (
	"context"
	"errors"

	"github.com/coinbase/rosetta-sdk-go/types"
	"gitlab.com/slbmax/flow-rosetta/configuration"
	"gitlab.com/slbmax/flow-rosetta/services/construction"
	AssetTypes "gitlab.com/slbmax/flow-rosetta/types"
)

// CallAPIService implements the server.CallAPIServicer interface.
type CallAPIService struct {
	config *configuration.Configuration
	client construction.Client
}

// NewCallAPIService creates a new instance of a CallAPIService.
func NewCallAPIService(cfg *configuration.Configuration, client construction.Client) *CallAPIService {
	return &CallAPIService{
		config: cfg,
		client: client,
	}
}

// Call implements the /call endpoint.
func (s *CallAPIService) Call(
	ctx context.Context,
	request *types.CallRequest,
) (*types.CallResponse, *types.Error) {
	if s.config.Mode != configuration.ModeOnline {
		return nil, AssetTypes.ErrUnavailableOffline
	}

	response, err := s.client.Call(ctx, request)
	if errors.Is(err, AssetTypes.ErrClientCallParametersInvalid) {
		return nil, AssetTypes.WrapErr(AssetTypes.ErrCallParametersInvalid, err)
	}
	if errors.Is(err, AssetTypes.ErrClientCallOutputMarshal) {
		return nil, AssetTypes.WrapErr(AssetTypes.ErrCallOutputMarshal, err)
	}
	if errors.Is(err, AssetTypes.ErrClientCallMethodInvalid) {
		return nil, AssetTypes.WrapErr(AssetTypes.ErrCallMethodInvalid, err)
	}
	if err != nil {
		return nil, AssetTypes.WrapErr(AssetTypes.ErrGeth, err)
	}

	return response, nil
}
