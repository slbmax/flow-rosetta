package services

import (
	"context"

	"github.com/coinbase/rosetta-sdk-go/server"
	"github.com/coinbase/rosetta-sdk-go/types"
	"gitlab.com/slbmax/flow-rosetta/configuration"
	"gitlab.com/slbmax/flow-rosetta/services/construction"
	AssetTypes "gitlab.com/slbmax/flow-rosetta/types"
)

// MempoolAPIService implements the server.MempoolAPIServicer interface.
type MempoolAPIService struct {
	config *configuration.Configuration
	client construction.Client
}

// NewMempoolAPIService creates a new instance of a MempoolAPIService.
func NewMempoolAPIService(
	config *configuration.Configuration,
	client construction.Client,
) server.MempoolAPIServicer {
	return &MempoolAPIService{
		config: config,
		client: client,
	}
}

// Mempool implements the /mempool endpoint.
func (s *MempoolAPIService) Mempool(
	ctx context.Context,
	_ *types.NetworkRequest,
) (*types.MempoolResponse, *types.Error) {
	return nil, AssetTypes.WrapErr(AssetTypes.ErrUnimplemented, nil)

	// TODO: Uncomment the following code when the txpool_content is implemented

	//if s.config.IsOfflineMode() {
	//	return nil, AssetTypes.ErrUnavailableOffline
	//}
	//
	//response, err := s.client.GetMempool(ctx)
	//if err != nil {
	//	return nil, AssetTypes.WrapErr(AssetTypes.ErrGeth, err)
	//}
	//
	//return response, nil
}

// MempoolTransaction implements the /mempool/transaction endpoint.
func (s *MempoolAPIService) MempoolTransaction(
	ctx context.Context,
	request *types.MempoolTransactionRequest,
) (*types.MempoolTransactionResponse, *types.Error) {
	return nil, AssetTypes.WrapErr(AssetTypes.ErrUnimplemented, nil)
}
